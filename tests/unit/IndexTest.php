<?php

namespace tests;

use PHPUnit\Framework\TestCase;

class IndexTest extends TestCase
{
    /**
     * @test
     */
    public function indexShouldReturnHelloWorld()
    {
        ob_start();
        require_once __DIR__ . '/../../index.php';
        $indexOutput = ob_get_contents();
        ob_end_clean();

        $this->assertSame("Hello world", $indexOutput);
    }
}
